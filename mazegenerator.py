import pygame, sys, time
from pygame.locals import QUIT
from collections import deque
from random import randint, seed

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)

class Cell():
    def __init__(self, x, y, size):
        self.x = x
        self.y = y
        self.size = size
        self.visited = False
        self.current = False
        self.start = False
        self.end = False

        self.went_up = False
        self.went_down = False
        self.went_left = False
        self.went_right = False
        self.rect = pygame.Rect(self.x, self.y, self.size, self.size)

    def draw(self, surface):
        if self.current:
            pygame.draw.rect(surface, RED, (self.x, self.y, self.size, self.size))
        else:
            pygame.draw.rect(surface, BLACK, (self.x, self.y, self.size, self.size))

        if self.start:
            pygame.draw.rect(surface, (0, 0, 255), (self.x, self.y, self.size, self.size))

        if self.end:
            pygame.draw.rect(surface, (0, 0, 255), (self.x, self.y, self.size, self.size))

        pygame.draw.rect(surface, WHITE, (self.x, self.y, self.size, self.size), 1)

        if self.went_up:
            pygame.draw.line(surface, BLACK, (self.x + 1, self.y), (self.x + self.size - 2, self.y), 1)

        if self.went_down:
            pygame.draw.line(surface, BLACK, (self.x + 1, self.y + self.size - 1), (self.x + self.size - 2, self.y + self.size - 1), 1)

        if self.went_left:
            pygame.draw.line(surface, BLACK, (self.x, self.y + 1), (self.x, self.y + self.size - 2), 1)

        if self.went_right:
            pygame.draw.line(surface, BLACK, (self.x + self.size - 1, self.y + 1), (self.x + self.size - 1, self.y + self.size - 2), 1)

pygame.init()

# can be set if we want to generate the same map over and over
#seed(5000)
ROW_COUNT = 20
COL_COUNT = 20

CELL_SIZE = 20
queue = deque()
rows = []
clock = pygame.time.Clock()

for y in range(ROW_COUNT):
    cols = []
    for x in range(COL_COUNT):
        cols.append(Cell(x * CELL_SIZE, y * CELL_SIZE, CELL_SIZE))
    rows.append(cols)

SURFACE = pygame.display.set_mode((COL_COUNT * CELL_SIZE, ROW_COUNT * CELL_SIZE), 0, 32)
pygame.display.set_caption('Maze Generator!')

start_row = randint(0, ROW_COUNT - 1)
start_col = randint(0, COL_COUNT - 1)

start_cell = rows[start_row][start_col]
start_cell.current = True
start_cell.visited = True
queue.append(start_cell)

first_run = True
start = time.time()

# initial draw of the entire board
for y in range(ROW_COUNT):
    for x in range(COL_COUNT):
        rows[y][x].draw(SURFACE)

# run the game loop
while True:
    clock.tick(60)

    adjacent_cells = []
    
    for y in range(ROW_COUNT):
        for x in range(COL_COUNT):
            cell = rows[y][x]

            if cell.current:
                possible_x = [x - 1, x + 1]
                possible_y = [y - 1, y + 1]

                if x == COL_COUNT - 1:
                    possible_x.remove(x + 1)
                elif x == 0:
                    possible_x.remove(x - 1)

                if y == ROW_COUNT - 1:
                    possible_y.remove(y + 1)
                elif y == 0:
                    possible_y.remove(y - 1)

                for x_cell in possible_x:
                    if not rows[y][x_cell].visited:
                        adjacent_cells.append(("left" if x_cell < x else "right", rows[y][x_cell]))

                for y_cell in possible_y:
                    if not rows[y_cell][x].visited:
                        adjacent_cells.append(("up" if y_cell < y else "down", rows[y_cell][x]))

    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()

    queue[-1].draw(SURFACE)
    pygame.display.update()

    if len(adjacent_cells) == 0:
        queue[-1].current = False
        queue[-1].draw(SURFACE)

        # if there are no adjacent cells and this is the last item
        # we're done, so set it as not current
        if len(queue) == 1:
            queue[-1].current = False
            queue[-1].draw(SURFACE)
            break
        
        queue.pop()
        queue[-1].current = True
        continue        

    # pick one of the adjacent cells at random
    next_cell_index = randint(0, len(adjacent_cells) - 1)
    next_cell = adjacent_cells[next_cell_index][1]
    next_cell.visited = True
    next_cell.current = True

    direction = adjacent_cells[next_cell_index][0]
    if direction == "up":
        queue[-1].went_up = True
        next_cell.went_down = True
    elif direction == "down":
        queue[-1].went_down = True
        next_cell.went_up = True
    elif direction == "left":
        queue[-1].went_left = True
        next_cell.went_right = True
    else:
        queue[-1].went_right = True
        next_cell.went_left = True

    queue[-1].current = False
    queue[-1].draw(SURFACE)
    next_cell.draw(SURFACE)

    queue.append(next_cell)

# set the start cell and end cell
start_cell = rows[0][0]
end_cell = rows[-1][-1]
start_cell.start = True
end_cell.end = True

start_cell.draw(SURFACE)
end_cell.draw(SURFACE)
pygame.display.update()
end = time.time()
print("Time taken to generate: " + str(end - start))
input('Press any key to exit')